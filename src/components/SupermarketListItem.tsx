import React from 'react';
import { Item } from './styles';
import { FaTrashAlt, FaEdit, FaRegCheckSquare, FaRegSquare } from 'react-icons/fa';
import { ISupermarketItem } from '../interfaces/supermarketItem.interface';
import { useDispatch } from 'react-redux';
import { deleteItem, editItem, updateItem } from '../actions';

interface SupermarketListItemProps {
    item: ISupermarketItem;
    openModal: () => void
}

const SupermarketListItem: React.FC<SupermarketListItemProps> = ({ item, openModal }) => {
    const dispatch = useDispatch();

    const removeItem = () => {
        dispatch(deleteItem(item.id));
    };

    const completeItem = () => {
        item.isComplete = !item.isComplete;
        dispatch(updateItem(item));
    };

    const edit = () => {
        item.isEditing = true;
        dispatch(editItem(item));
        openModal();
    };

    return (
        <Item>
            <div className={item.isComplete ? 'complete' : ''}>{item.name}</div>
            <div>
                {!item.isComplete ? (
                    <FaRegSquare onClick={completeItem} className="icon" />
                ) : (
                    <FaRegCheckSquare onClick={completeItem} className="icon" />
                )}
                <FaEdit className="icon" onClick={edit} />
                <FaTrashAlt className="icon" onClick={removeItem} />
            </div>
        </Item>
    );
};

export default SupermarketListItem;
