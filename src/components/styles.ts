import styled from 'styled-components';

interface Props {
    width?: number;
}

export const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    width: 100%;
    padding: 20px;
`;

export const Box = styled.div`
    background: #f5f5f5;
    width: 480px;
    min-height: 200px;
    border: 0.5px solid #6a737d;
    text-align: center;
    padding: 20px;

    h1 {
        font-size: 25px;
        font-weight: 600;
        margin: 0;
        padding: 0;
    }
`;

export const Item = styled.div`
    width: 100%;
    max-width: 380px;
    margin: 0 auto 10px;
    padding: 10px;
    background-color: #fff;
    font-size: 13px;
    border-radius: 4px;
    box-shadow: #ddd 0px 2px 1px 0px;
    justify-content: space-between;
    align-items: center;
    display: flex;
    font-weight: 600;

    .icon {
        cursor: pointer;
        color: #00a8ff;
        font-size: 14px;
        margin-left: 10px;
    }

    .complete {
        text-decoration: line-through;
    }
`;

export const Count = styled.div`
    font-size: 11px;
    color: #979595;
    margin-bottom: 20px;
    margin-top: 5px;
    text-transform: uppercase;
    font-weight: 600;
`;

export const ModalWrapper = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.4);
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const ModalContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    background-color: #fefefe;
    padding: 20px;
    border: 1px solid #888;
    border-radius: 8px;
    width: 290px;
    text-align: center;

    h2 {
        font-weight: 600;
        font-size: 18px;
        margin: 0;
        padding: 0;
    }

    input[type='text'] {
        border: 1px solid #d6d6d6;
        border-radius: 4px;
        padding: 10px;
        color: #333;
        margin-bottom: 10px;
        background: transparent;
        width: 100%;
        margin-top: 15px;
        outline: none;
        font-family: 'Montserrat', sans-serif;
        font-size: 12px;

        &:focus {
            border: 1px solid #888;
        }
    }

    .error {
        font-weight: 600;
        font-size: 12px;
        text-align: right;
        color: #cc0000;
        margin-bottom: 5px;
    }
`;

export const Button = styled.button`
    width: 100px;
    cursor: pointer;
    padding: 8px 10px;
    margin: 10px 0;
    font-size: 13px;
    border-radius: 4px;
    text-align: center;
    outline: none;
    max-width: 380px;

    &:hover {
        background-color: #d6d6d6;
    }
`;

export const PrimaryButton = styled(Button)`
    background-color: #00a8ff;
    color: #fff;
    border: 1px solid #018fd9;
    width: ${({ width }: Props) => (width ? `${width}%` : '100px')};

    &:hover {
        background-color: #018fd9;
    }

    &:disabled {
        background-color: #d3d3d3;
        border: 1px solid #d3d3d3;
        cursor: default;
    }
`;

export const ButtonHolder = styled.div`
    display: flex;
    justify-content: space-between;
`;
