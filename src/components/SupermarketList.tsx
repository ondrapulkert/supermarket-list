import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ISupermarketItem } from '../interfaces/supermarketItem.interface';
import { AppStore } from '../reducers';
import SupermarketListItem from './SupermarketListItem';
import { Wrapper, Box, Count, PrimaryButton } from './styles';
import Modal from './Modal';
import { cancelEditItem } from '../actions';
// @ts-ignore
import Pluralize from 'react-pluralize';

const SupermarketList: React.FC = () => {
    const list: ISupermarketItem[] = useSelector((state: AppStore) => state.supermarket.list);
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const dispatch = useDispatch();

    const openModal = (addItem?: boolean) => {
        if (addItem) {
            dispatch(cancelEditItem());
        }

        setIsModalOpen(true);
    };

    const onCancelClick = () => {
        setIsModalOpen(false);
        dispatch(cancelEditItem());
    };

    return (
        <>
            <Wrapper>
                <Box>
                    <h1>Supermarket List</h1>
                    <Count>
                        <Pluralize singular={'item'} plural={'items'} count={list.length} />
                    </Count>
                    {!list.length && <div>No items yet.</div>}
                    {list &&
                        list.map((item: ISupermarketItem, index: number) => {
                            return <SupermarketListItem openModal={openModal} key={index} item={item} />;
                        })}
                    <PrimaryButton type="submit" onClick={() => openModal(true)} width={100}>
                        Add Item
                    </PrimaryButton>
                </Box>
            </Wrapper>
            {isModalOpen && <Modal onCancelClick={onCancelClick} />}
        </>
    );
};

export default SupermarketList;
