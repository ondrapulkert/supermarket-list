import React, { useEffect, useState } from 'react';
import { ModalWrapper, ModalContainer, Button, ButtonHolder, PrimaryButton } from './styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { v4 } from 'uuid';
import { addItem, updateItem } from '../actions';
import { ISupermarketItem } from '../interfaces/supermarketItem.interface';
import { AppStore } from '../reducers';

interface ModalProps {
    onCancelClick: () => void;
}

const validationSchema = Yup.object({
    title: Yup.string()
        .min(5, 'Min characters is 5.')
        .required('This field is required.'),
});

const Modal: React.FC<ModalProps> = ({ onCancelClick }) => {
    const dispatch = useDispatch();
    const list: ISupermarketItem[] = useSelector((state: AppStore) => state.supermarket.list);
    const [editing, setEditing] = useState<{ state: boolean; value: ISupermarketItem }>({
        state: false,
        value: { id: '', name: '', isComplete: false, isEditing: false },
    });

    useEffect(() => {
        if (list.length) {
            list.forEach((item: ISupermarketItem) => {
                if (item.isEditing) {
                    setEditing({ state: true, value: item });
                }
            });
        }
    }, [list]);

    const { handleSubmit, handleChange, errors, values, isValid } = useFormik({
        enableReinitialize: true,
        initialValues: {
            title: editing.value.name !== '' ? editing.value.name : '',
        },
        validationSchema,
        onSubmit: values => {
            if (editing.state) {
                dispatch(updateItem({ ...editing.value, name: values.title }));
            } else {
                dispatch(addItem({ id: v4(), name: values.title, isComplete: false, isEditing: false }));
            }
            onCancelClick();
        },
    });

    return (
        <ModalWrapper>
            <ModalContainer>
                <h2>Add Item</h2>
                <form onSubmit={handleSubmit}>
                    <input id="title" name="title" type="text" onChange={handleChange} value={values.title} />
                    {errors.title ? <div className="error">{errors.title}</div> : null}
                    <ButtonHolder>
                        <Button onClick={() => onCancelClick()}>Cancel</Button>
                        <PrimaryButton type="submit" disabled={!isValid}>
                            {editing.state ? 'Edit' : 'Add'}
                        </PrimaryButton>
                    </ButtonHolder>
                </form>
            </ModalContainer>
        </ModalWrapper>
    );
};

export default Modal;
