export interface ISupermarketItem {
    id: string;
    name: string;
    isComplete: boolean;
    isEditing: boolean;
}