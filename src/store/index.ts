import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer, { AppStore } from '../reducers';
import thunk from 'redux-thunk';
import { loadState, saveState } from './localStorage';

const persistedState = loadState();
const store = createStore<AppStore, any, any, any>(
    rootReducer,
    persistedState,
    composeWithDevTools(applyMiddleware(thunk))
);

store.subscribe(() => {
    saveState({
        supermarket: store.getState().supermarket,
    });
});

export default store;
