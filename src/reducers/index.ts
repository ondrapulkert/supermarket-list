import { combineReducers } from "redux";
import { ISupermarketState } from "./types";
import { supermarketReducer } from "./supermarketReducer";

export interface AppStore {
  supermarket: ISupermarketState;
}

const rootReducer = combineReducers({
  supermarket: supermarketReducer
});

export default rootReducer;
