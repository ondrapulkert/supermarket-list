import {ISupermarketItem} from "../interfaces/supermarketItem.interface";

export interface ISupermarketState {
    list: ISupermarketItem[];
}
