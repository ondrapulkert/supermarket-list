import { ISupermarketState } from './types';
import { v4 } from 'uuid';
import { SupermarketActionTypes } from '../actions/types';

const initState: ISupermarketState = {
    list: [
        {
            id: v4(),
            name: 'first',
            isComplete: false,
            isEditing: false,
        },
    ],
};

export function supermarketReducer(
    state: ISupermarketState = initState,
    action: SupermarketActionTypes
): ISupermarketState {
    switch (action.type) {
        case 'ADD_ITEM': {
            return {
                ...state,
                list: [...state.list, action.payload],
            };
        }
        case 'DELETE_ITEM': {
            return {
                ...state,
                list: state.list.filter(item => item.id !== action.payload),
            };
        }
        case 'UPDATE_ITEM': {
            return {
                ...state,
                list: state.list.map(item => {
                    if (item.id !== action.payload.id) {
                        return item;
                    }
                    return action.payload;
                }),
            };
        }

        case 'EDIT_ITEM': {
            return {
                ...state,
                list: state.list.map(item => {
                    if (item.id !== action.payload.id) {
                        return { ...item, isEditing: false };
                    }
                    return action.payload;
                }),
            };
        }

        case 'CANCEL_EDIT_ITEM': {
            return {
                ...state,
                list: state.list.map(item => {
                    return { ...item, isEditing: false };
                }),
            };
        }

        default:
            return state;
    }
}
