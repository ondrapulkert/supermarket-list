import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import SupermarketList from './components/SupermarketList';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
    }
    body {
        font-family: 'Montserrat', sans-serif;
        background: #DDDDDD;
    }
`;

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <SupermarketList />
            <GlobalStyle />
        </Provider>
    );
};

export default App;
