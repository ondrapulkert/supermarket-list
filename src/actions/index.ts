import { ISupermarketItem } from '../interfaces/supermarketItem.interface';
import { ADD_ITEM, CANCEL_EDIT_ITEM, DELETE_ITEM, EDIT_ITEM, SupermarketActionTypes, UPDATE_ITEM } from './types';

export const addItem = (item: ISupermarketItem): SupermarketActionTypes => {
    return {
        type: ADD_ITEM,
        payload: item,
    };
};

export const deleteItem = (id: string): SupermarketActionTypes => {
    return {
        type: DELETE_ITEM,
        payload: id,
    };
};

export const updateItem = (item: ISupermarketItem): SupermarketActionTypes => {
    return {
        type: UPDATE_ITEM,
        payload: item,
    };
};

export const editItem = (item: ISupermarketItem): SupermarketActionTypes => {
    return {
        type: EDIT_ITEM,
        payload: item,
    };
};

export const cancelEditItem = (): SupermarketActionTypes => {
    return {
        type: CANCEL_EDIT_ITEM,
    };
};
