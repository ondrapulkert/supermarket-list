import { ISupermarketItem } from '../interfaces/supermarketItem.interface';

export const ADD_ITEM = 'ADD_ITEM';
export const DELETE_ITEM = 'DELETE_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';
export const CANCEL_EDIT_ITEM = 'CANCEL_EDIT_ITEM';

interface AddItemAction {
    type: typeof ADD_ITEM;
    payload: ISupermarketItem;
}
interface DeleteItemAction {
    type: typeof DELETE_ITEM;
    payload: string;
}
interface UpdateItemAction {
    type: typeof UPDATE_ITEM;
    payload: ISupermarketItem;
}
interface EditItemAction {
    type: typeof EDIT_ITEM;
    payload: ISupermarketItem;
}
interface CancelEditAction {
    type: typeof CANCEL_EDIT_ITEM;
}
export type SupermarketActionTypes =
    | AddItemAction
    | DeleteItemAction
    | UpdateItemAction
    | EditItemAction
    | CancelEditAction;
